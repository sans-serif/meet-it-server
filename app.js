import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import { ApolloServer } from 'apollo-server-express';

import { authMiddleware } from './src/middleware/authMiddleware';
import { typeDefs } from './src/typeDefs/typeDefs';
import { resolvers } from './src/resolvers/resolvers';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => authMiddleware(req)
});

server.applyMiddleware({ app });

module.exports = app;
