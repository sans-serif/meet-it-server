import { gql } from 'apollo-server-express';

export const typeDefs = gql`
    type User {
        _id: ID!
        email: String!
        password: String
        createdEvents: [Event!]
        bookedEvents: [Event!]
    }

    type AuthData {
        userId: ID!
        token: String!
        tokenExpiration: Int!
    }

    type Event {
        _id: ID!
        title: String!
        description: String!
        canceled: Boolean!
        price: Float!
        date: String!
        creator: User!
        belongsToCurrentUser: Boolean!
        wasBookedByCurrentUser: Boolean!
    }

    type Booking {
        _id: ID!
        event: Event!
        user: User!
        createdAt: String!
        updatedAt: String!
    }

    input EventInput {
        title: String!
        description: String!
        price: Float!
        date: String!
    }

    input UserInput {
        email: String!
        password: String!
    }

    type Query{
        events: [Event!]!
        event(eventId: ID!): Event!
        activeEvents: [Event!]!
        userEvents: [Event!]!
        bookings: [Booking!]!
        booking(eventId: ID!): Booking!
        user: User!
    }

    type Mutation {
        signUp(userInput: UserInput): User
        signIn(email: String!, password: String!): AuthData!
        createEvent(eventInput: EventInput): Event
        cancelEvent(eventId: ID!): Event!
        bookEvent(eventId: ID!): Booking!
        cancelBooking(bookingId: ID!): Booking!
    }
`;
