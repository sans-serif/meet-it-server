import { signIn, signUp, user } from './auth';
import { events, event, userEvents, activeEvents, createEvent, cancelEvent } from './events'
import { bookings, booking, bookEvent, cancelBooking } from './booking';

export const resolvers = {
    Query: {
        events,
        activeEvents,
        userEvents,
        bookings,
        booking,
        event,
        user
    },
    Mutation: {
        signIn,
        signUp,
        createEvent,
        cancelEvent,
        bookEvent,
        cancelBooking
    }
};


