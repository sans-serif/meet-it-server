import Booking from '../models/booking';
import Event from '../models/event';
import User from '../models/user';
import { transformBookings } from './merge';

const bookings = async (root, args, context) => {
    if (!context.isAuth) {
        throw new Error('Unauthenticated');
    }
    try {
        const bookings = await Booking.find({ user: context.userId });
        return bookings.map(booking => {
            return transformBookings(booking);
        })
    } catch (e) {
        throw e;
    }
};

const booking = async (root, args, context) => {
    if (!context.isAuth) {
        throw new Error('Unauthenticated');
    }
    try {
        const booking = await Booking.findOne({ user: context.userId, event: args.eventId });
        return transformBookings(booking, context.userId)
    } catch (e) {
        throw e;
    }
};

const bookEvent = async (root, args, context) => {
    if (!context.isAuth) {
        throw new Error('Unauthenticated');
    }

    try {
        const user = await User.findById(context.userId);
        const fetchedEvent = await Event.findOne({ _id: args.eventId });

        if (!user) {
            console.log('User not found');
            return false;
        }

        if (fetchedEvent.canceled) {
            throw new Error('Event has been canceled');
        }

        if (fetchedEvent.creator._id.equals(context.userId)) {
            throw new Error('Not authorized');
        }

        if (user.bookedEvents.includes(fetchedEvent._id)) {
            throw new Error('Event already has booked');
        }

        const booking = new Booking({
            user: context.userId,
            event: fetchedEvent
        });

        const result = await booking.save();
        const createdBook = transformBookings(result);

        user.bookedEvents.push(fetchedEvent);
        await user.save();

        return createdBook;
    } catch (e) {
        throw e;
    }

};

const cancelBooking = async (root, args, context) => {
    if (!context.isAuth) {
        throw new Error('Unauthenticated');
    }
    try {
        const booking = await Booking.findById(args.bookingId);
        const user = await User.findById(booking.user._id);
        const event = await Event.findById(booking.event._id);

        await Booking.deleteOne({ _id: args.bookingId });

        user.bookedEvents = user.bookedEvents.filter(ev => ev._id === event._id);

        user.save((err) => {
            if (err) {
                throw  new (err);
            }
        });

        return booking;
    } catch (e) {
        console.log(e);
        throw e;
    }
};

export { bookings, booking, bookEvent, cancelBooking }
