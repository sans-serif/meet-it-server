import User from '../models/user';
import Event from '../models/event';
import { transformEvent } from './merge';

const events = async (root, args, context) => {
    try {
        const events = await Event.find();
        return events.map(event => transformEvent(event, context.userId));
    } catch (e) {
        throw e;
    }
};

const activeEvents = async (root, args, context) => {
    try {
        const events = await Event.find({ canceled: { $eq: false } });
        return events.map(event => transformEvent(event, context.userId));
    } catch (e) {
        throw (e);
    }
};

const event = async (root, args, context) => {
    try {
        const event = await Event.findById(args.eventId);
        return transformEvent(event, context.userId)
    } catch (e) {
        throw e;
    }
};

const userEvents = async (root, args, context) => {
    if (!context.userId) {
        throw new Error('Unauthenticated');
    }

    try {
        const events = await Event.find({ creator: context.userId });
        return events.map(event => transformEvent(event, context.userId));
    } catch (e) {
        throw e;
    }
};

const createEvent = async (root, args, context) => {
    if (!context.isAuth) {
        throw new Error('Unauthenticated');
    }
    const event = new Event({
        title: args.eventInput.title,
        description: args.eventInput.description,
        canceled: false,
        price: +args.eventInput.price,
        date: new Date(args.eventInput.date),
        creator: context.userId
    });
    let createdEvent;
    try {
        const result = await event.save();
        createdEvent = transformEvent(result);
        const creator = await User.findById(context.userId);

        if (!creator) {
            console.log('User not found');
            return false;
        }
        creator.createdEvents.push(event);
        await creator.save();

        return createdEvent;
    } catch (e) {
        throw e;
    }
};

const cancelEvent = async (root, args, context) => {
    if (!context.isAuth) {
        throw new Error('Unauthenticated');
    }

    try {
        const creator = await User.findById(context.userId);
        const canceledEvent = await Event.findById(args.eventId);

        if (!creator.createdEvents.includes(canceledEvent._id)) {
            throw new Error('Not authorized');
        }

        canceledEvent.canceled = true;
        canceledEvent.save((err) => {
            if (err) {
                throw  new (err);
            }
        });

        return canceledEvent;
    } catch (e) {
        console.log(e);
        throw  e;
    }
};

export { events, event, userEvents, activeEvents, createEvent, cancelEvent }

