import DataLoader from 'dataloader';
import { dateToString } from '../helpers/date';

import User from '../models/user';
import Event from '../models/event';
import Booking from '../models/booking';

const eventLoader = new DataLoader(eventIds => {
    return events(eventIds);
});

const userLoader = new DataLoader(userIds => {
    return User.find({ _id: { $in: userIds } });
});

const events = async eventIds => {
    try {
        const events = await Event.find({ _id: { $in: eventIds } });
        events.sort((a, b) => {
            return (
                eventIds.indexOf(a._id.toString()) - eventIds.indexOf(b._id.toString())
            );
        });
        return events.map(event => {
            return transformEvent(event);
        });
    } catch (e) {
        throw e;
    }
};

const bookings = async (event, userId) => {
  try {
      const bookingsList = await Booking.find({ user: userId });

      return bookingsList.some(booking => {
          return booking.event._id.toString() === event._id.toString();
      })

  }  catch (e) {
      throw e;
  }
};

const singleEvent = async eventId => {
    try {
        return await eventLoader.load(eventId.toString());
    } catch (e) {
        throw e;
    }
};

const user = async userId => {
    try {
        const user = await userLoader.load(userId.toString());

        return {
            ...user._doc,
            password: null,
            createdEvents: () => eventLoader.loadMany(user._doc.createdEvents)
        }
    } catch (e) {
        throw e;
    }
};

const transformEvent = (event, currentUser) => {
    return {
        ...event._doc,
        date: dateToString(event.date),
        creator: user.bind(this, event.creator),
        belongsToCurrentUser: event.creator.toString() === currentUser,
        wasBookedByCurrentUser: bookings.bind(this, event, currentUser)
    }
};

const transformBookings = booking => {
    return {
        ...booking._doc,
        user: user.bind(this, booking._doc.user),
        event: singleEvent.bind(this, booking._doc.event._id),
        createdAt: dateToString(booking._doc.createdAt),
        updatedAt: dateToString(booking._doc.updatedAt)
    }
};

export { transformEvent, transformBookings }
