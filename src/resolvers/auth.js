import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import User from '../models/user';

const signUp = async (root, args) => {
    try {
        console.log(args);

        const existingUser = await User.findOne({ email: args.userInput.email });

        if (existingUser) {
            console.log('User exists already.');
            return false;
        }

        const hashedPassword = await bcrypt.hash(args.userInput.password, 12);

        const user = new User({
            email: args.userInput.email,
            password: hashedPassword
        });

        const result = await user.save();

        return { ...result._doc, password: null };
    } catch (e) {
        throw e;
    }
};

const signIn = async (parent, { email, password }) => {
    const user = await User.findOne({ email: email });
    if (!user) {
        throw new Error('User does not exist!');
    }

    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
        throw new Error('Password is incorrect!');
    }

    const token = jwt.sign({ userId: user.id, email: user.email }, 'somesupersecretkey', {
        expiresIn: '1h'
    });

    return {
        userId: user.id,
        token: token,
        tokenExpiration: 1
    }
};

const user = async (root, args, context) => {
    if (!context.userId) {
        throw new Error('Unauthenticated');
    }
    try {
        const user = await User.findOne({ _id: context.userId });
        if (!user) {
            throw new Error('User does not exist!');
        }
        return user;
    } catch (e) {
        throw e;
    }
};

export { signUp, signIn, user }
