const jwt = require('jsonwebtoken');

export const authMiddleware = (req) => {
    try {

        const authHeader = req.headers.authorization || '';
        const token = authHeader.split(' ')[1];
        const decodedToken = jwt.verify(token, 'somesupersecretkey');
        return {
            isAuth: true,
            userId: decodedToken.userId
        }
    } catch (e) {
        return {
            isAuth: false
        }
    }
};
